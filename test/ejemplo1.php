<?php

interface PersonaInterface{
    public function mostrarNombre();
    public function mostrarCorreoElectronico();
}

abstract class Persona implements PersonaInterface{
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    protected $correoElectronico;
    public $cumpleanios;
    function __construct($nombre, $apellidoPaterno, $apellidoMaterno, $correoElectronico, $cumpleanios)
    {
        $this->nombre=$nombre;
        $this->apellidoPaterno=$apellidoPaterno;
        $this->apellidoMaterno=$apellidoMaterno;
        $this->correoElectronico=$correoElectronico;
        $this->cumpleanios=$cumpleanios;
    }
    public function mostrarNombre()
    {
        $nombreCompleto=$this->nombre . " " . $this->apellidoPaterno . " " . $this->apellidoMaterno;

        return $nombreCompleto;
    }

    public function mostrarCorreoElectronico()
    {
        return $this->correoElectronico;
    }

    abstract public function mostrarCumpleanios();
    
}


class Alumno extends Persona{
    private $calificacion;

    public function __construct($nombre, $apellidoPaterno, $apellidoMaterno, $correoElectronico,$cumpleanios, $calificacion)
    {
        $this->calificacion=$calificacion;      
        parent::__construct($nombre, $apellidoPaterno, $apellidoMaterno, $correoElectronico, $cumpleanios);
    }
    public function mostrarCumpleanios()
    {
        $cumpeanios= "Mi cumple es: " . $this->cumpleanios;

        return $cumpeanios;
    }
}

class Docente extends Persona{
    private $salario;

    public function __construct($nombre, $apellidoPaterno, $apellidoMaterno, $correoElectronico, $cumpleanios, $salario)
    {
        $this->salario=$salario;
        parent::__construct($nombre, $apellidoPaterno, $apellidoMaterno, $correoElectronico, $cumpleanios);
    }
    public function mostrarCumpleanios()
    {
        $cumpeanios= "¿Ya es mi cumpleaños?" . $this->cumpleanios;

        return $cumpeanios;
    }
}
$alumno=new Alumno('Juan', 'Perez','Hernandez','juan@gmail.com','1983-01-23',90);
$docente=new Docente('Marcos', 'Mendoza','Hernandez','marquitos@gmail.com','1997-03-14',20000);

echo "Datos del alumno\n";
echo "Nombre" . $alumno->mostrarNombre() . "\n";
echo "Correo" . $alumno->mostrarCorreoElectronico() . "\n";
echo $alumno->mostrarCumpleanios() . "\n\n";
echo "Datos del docente\n";
echo "Nombre" . $docente->mostrarNombre() . "\n";
echo "Correo" . $docente->mostrarCorreoElectronico() . "\n";
echo $docente->mostrarCumpleanios();
?>